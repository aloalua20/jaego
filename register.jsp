<!- 상품등록 ->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType = "text/html; charset=utf-8"%>
<%@ page import="java.sql.*,javax.sql.*, java.io.*"%>
<html>
<title>상품 등록</title>
<head>
<!--에러 잡는 try-catch문-->
<%!      
   public static int extractErrorCode (SQLException sqlException) {
      int errorCode = sqlException.getErrorCode();
      SQLException nested = sqlException.getNextException();
      while(errorCode==0&&nested!=null){
         errorCode=nested.getErrorCode();
         nested=nested.getNextException();
      }
      return errorCode;
   }
%>
<%
//날짜 받아오기
 java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd");
 String today = formatter.format(new java.util.Date());

 //out.println(today);

%>
<style type = "text/css">
  table {
	margin:auto;
    text-align: center;
    
    border: 5px 
  
  }
h2 {
   text-align:center;
}   
</style>
</head>
<br>
<h2>㈜트와이스 재고 현황-상품등록</h2>
<body>
<%
//MySQL드라이버 로딩: class클래스의 forName()메소드를 이용하여 interface driver 로드
	Class.forName("com.mysql.jdbc.Driver");
//MySQL Connection 객체 생성
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/seradatabase", "root","aeae");
//sql쿼리 생성/실행을 위한 statement객체 생성
	Statement stmt = conn.createStatement();
//새 상품의 재고번호 지정 (기존 마지막 상품번호+1)
try{	
	int id = 0;
	ResultSet rset = stmt.executeQuery("select max(id) from jaego;");
	rset.next();
	id = rset.getInt(1)+1;
	
%>

<table border = 1 cellpadding=5>
<!--from 태그에서, method는 post, enctype은 multipart/form-data로 지정
method 속성: 서버에 요청 방식을 지정하는 것, 생략 시 default값인 get사용,
			파일 업로드 시에는 반드시 post로 지정
enctype 속성: 서버에 폼 데이터 전송 시 인코딩 방식을 지정하는 것,
			파일 업로드를 위해 파일 입력 받음. 이 경우 input태그의 type속성은 file.-->
<form method=post action="write.jsp" enctype="multipart/form-data">
<!-- info에 register값을 넣어 write.jsp로 전달 -->
<input type=hidden name="info" value="register">
<input type=hidden name="id" value="<%=id%>">
	<tr>
	<td width=120>상품 번호 </td>
	<td width=480 align=left><input type=number name='id' value=<%=id%> readonly></input></td>
	</tr>
	<tr>										
	<td width=120>상품명 </td>
	<td width=480 align=left><input type=text name ='name' required></input></td>
	</tr>
	<tr>
	<td width=120>재고 현황 </td>
	<td width=480 align=left><input type=number name='no' required></input></td>
	</tr>
	<tr>
	<td width=120>상품등록일 </td>
	<td width=480 align=left><input type='hidden' name='productDay' value="<%=today%>" required><%=today%></input></td>
	</tr>
	<tr>
	<td width=120>재고등록일 </td>
	<td width=480 align=left><input type='hidden' name='stockDay' value="<%=today%>" required><%=today%></input></td>
	</tr>
	<tr>
	<td width=120>상품설명 </td>
	<td width=480 align=left><input type=text name='content' required></input></td>
	</tr>
	<tr>
	<td width=120>상품사진 </td>
	<td width=480 align=left>
	<input type=file name="uploadFile">
	</td>
	</tr>
</table>
<br>
<table>
	<tr>
	<td><input type=submit value = '완료'></input></td>
	<td><input type=button onClick=location.href='list.jsp' value = '취소'> </input></td>
	</tr>
</table>
</form>
<%
	rset.close();
	stmt.close();
	conn.close();
} catch(SQLException e) {
      if(extractErrorCode(e)==1062){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("데이터가 중복됩니다.");
         }else if(extractErrorCode(e)==1146){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("테이블이 존재하지 않습니다.");
         }else{
            out.println("[기타] <br>");
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.");
            out.println("입력값이 없습니다.");
         }
}
%>
</body>
</html>
