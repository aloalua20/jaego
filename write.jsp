<!- 실제 상품 등록, 재고 수정하는 파일 ->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType = "text/html; charset=utf-8"%>
<%@ page import="java.sql.*,javax.sql.*, java.io.*"%>
<!-- MultipartRequest를 사용하기 위한 패키지 임포트-->
<%@page import="com.oreilly.servlet.MultipartRequest" %>
<%@page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy" %>

<html>
<head>

<style type = "text/css">
table {
   margin:auto;
   text-align: center;
}   
h2 {
   text-align:center;
}   
</style>
</head>
<body>

<%
	request.setCharacterEncoding("UTF-8");
	
	//파일 경로
	String savePath = "./file";
	//현재 jsp 페이지의 웹 app 상의 절대 경로 구하기
	ServletContext context = getServletContext();
	String realPath = context.getRealPath(savePath);
	//인코딩 타입
	String format = "UTF-8";
	//업로드될 파일의 최대 크기
	int maxSize = 5*1024*1024;
	/*파일 업로드를 수행하는 MultipartRequest클래스 생성자
	request: request 객체, 
	realPath: 파일 업로드할 폴더, 
	maxSize: 업로드할 파일의 최대 크기, 
	format: 인코딩 방식, 
	DefaultFileRenamePolicy(): 같은 파일 덮어쓰기 방지 설정 */
	MultipartRequest multi = new MultipartRequest(request, realPath, maxSize, format, new DefaultFileRenamePolicy());

	/*MultipartRequest 사용시, tomcat의 request객체의 getParameter메서드 사용 불가 
	∴ multi객체의 getParameter 메서드로 인자 가져오기 */
	String id = multi.getParameter("id");
	String name = multi.getParameter("name");
	String no = multi.getParameter("no");
	String stockDay = multi.getParameter("stockDay");
	String productDay = multi.getParameter("productDay");
	String content = multi.getParameter("content");
	
	String info = multi.getParameter("info");

	//MySQL드라이버 로딩: class클래스의 forName()메소드를 이용하여 interface driver 로드
	Class.forName("com.mysql.jdbc.Driver");
	
	//MySQL Connection 객체 생성
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/seradatabase", "root","aeae");

	//info 값이 edit일 경우 > 재고 수정 
	if("edit".equals(info)){
			
		String sql = "update jaego set no=?, stockDay=? where id = ?;";

		PreparedStatement pstmt = conn.prepareStatement(sql);
				
		pstmt.setString(1, no);
		pstmt.setString(2, stockDay); 
		pstmt.setString(3, id);
			
		pstmt.executeUpdate();
		pstmt.close();
			
		out.println("id="+id);
		out.println("stockDay="+productDay);
		out.println("productDay="+stockDay);
		out.println("no="+content);
		
	//info 값이 register일 경우 > 상품 등록
	} else if("register".equals(info)){

		// 파일 이름 받아오기
		String uploadFile = multi.getFilesystemName("uploadFile");
		
		//파일 경로 = 절대경로 + 파일이름 
		File file = new File(realPath + "/"+ uploadFile);
		//파일 경로 = /파일명
		String path = "/"+ uploadFile;
		
		String sql = "insert into jaego(id, name, no, stockDay, productDay, content, path) values(?, ?, ?, ?, ?, ? ,?)";
		PreparedStatement pstmt = conn.prepareStatement(sql);
	
		pstmt.setString(1, id);
		pstmt.setString(2, name);
		pstmt.setString(3, no);
		pstmt.setString(4, stockDay); 
		pstmt.setString(5, productDay); 
		pstmt.setString(6, content); 
		pstmt.setString(7, path);
		
		pstmt.executeUpdate();
		pstmt.close();

		}
	
%>

<%
	conn.close();
	//write 파일에서 수정/등록 후 view.jsp 화면 출력
	out.println("<script type=\"text/javascript\"> location.href=\"view.jsp?id=" + id + "\"; </script>");
%>

</body>
</html>