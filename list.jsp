<!- 전체 재고 현황 ->

<!-hmtl에서 한글 설정, 브라우저에게 encoding형식 알림->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<!-jsp에서 한글 설정->
<%@ page contentType = "text/html; charset=utf-8"%>
<!-jsp로 자바 파일 임포트
		java.sql.*: sql에 관련된 모든 자바 클래스 임포트
		javax.sql.*: JDBC 확장 클래스
		java.io.*: java.io 패키지, 데이터 입출력(input,output,reader,writer->
<%@ page import="java.sql.*,javax.sql.*, java.io.*"%>
<html>
<head>
<!--에러 잡는 try-catch문-->
<%!      
   public static int extractErrorCode (SQLException sqlException) {
      int errorCode = sqlException.getErrorCode();
      SQLException nested = sqlException.getNextException();
      while(errorCode==0&&nested!=null){
         errorCode=nested.getErrorCode();
         nested=nested.getNextException();
      }
      return errorCode;
   }
%>
<title>㈜트와이스 재고 현황-전체현황</title>
<style type = "text/css">
  table {
	margin:auto;
    text-align: center;

    border: 5px 
  
  }
h2 {
   text-align:center;
}   
</style>
<h2>㈜트와이스 재고 현황-전체현황</h2>
</head>
<br>
<body>
<table border = 1 width=600>
	<tr>
	<td width=120>상품번호 </td>
	<td width=280>상품명 </td>
	<td width=200>현재 재고수 </td>
	<td width=200>재고파악일 </td>
	<td width=200>상품등록일 </td>
	</tr>
<%
try{
int totaldata = 0;  // 전체 데이터 수
int countpage = 5;    //한화면에 나올 페이지번호 수 
int datalist = 5;   //출력될 데이터 수
int totalpage = 0;     //전체 페이지 수
int linecount = 0;
// 페이지 변경시 시작 번호를 변경하기 위한 변수 계산  
String starts = request.getParameter("togo"); 

int start;                                            

try{
	// getParameter는 문자열로 인자를 받기 때문에 int형으로 형변환이 필요하다
     start = Integer.parseInt(starts);        
} catch (Exception e){
	//try catch 구문을 활용하여 초기 변수값이 없어 발생하는 오류를 응용, start 에 초기 값을 부여하였다.
     start = 0;      //현재페이지                    
}
if(start <0){
       start = 0;
}  

//MySQL드라이버 로딩: class클래스의 forName()메소드를 이용하여 interface driver 로드
	Class.forName("com.mysql.jdbc.Driver");
//MySQL Connection 객체 생성
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/seradatabase", "root","aeae");
//sql쿼리 생성/실행을 위한 statement객체 생성
	Statement stmt = conn.createStatement();
	Statement stmt2 = conn.createStatement();
//select sql쿼리 실행 결과물을 담는 ResultSet rset 생성
	ResultSet rset = stmt.executeQuery("select * from jaego order by id asc;");
	while(rset.next()){
		int id = rset.getInt(1);
			// ~ 번부터 ~번 까지 출력
      if (linecount < start){         
          linecount++; 
          continue;
      }
%>
	<tr>
	<td><%=id%></td>
	<!--제목 누르면 id 번호에 따른 글 조회-->
	<td><a href='view.jsp?id=<%=id%>'><%=rset.getString(2)%></a></td>
	<td><%=rset.getInt(3)%></td>
	<td><%=rset.getDate(4)%></td>
	<td><%=rset.getDate(5)%></td>
	</tr>
	
<%
	linecount++;
	
	if(linecount >= start+datalist){
           break;
     }
	}
	ResultSet rset2 = stmt2.executeQuery("select count(*) from jaego;");
	rset2.next();
   totaldata = rset2.getInt(1); // 전체 데이처 수 계산

	//총페이지 수 계산
   if(totaldata != 0){
      if((totaldata % datalist) == 0){
         totalpage = (totaldata/datalist);
      }else{
         totalpage = (totaldata/datalist) + 1;
      } 
   }      
   
   if(start >= (totalpage*datalist)-datalist){
      start = (totalpage*datalist)-datalist;
    }   // 맨마지막 페이지를 클릭 했을때 다음 페이지로 넘어가못하도록 출력되는 페이지의 한계숫자를 조정
	String first = "처음";
    String before = "이전";    
    String nex = "다음";
    String over = "끝";
	//클릭한 번호에 따라 보이는 페이지 번호의 시작과 끝을 변경하는 식
     int startpage = (start/(countpage*datalist)) * countpage +1;  
     int endpage = startpage + countpage-1;
    // 맨마지막 페이지의 번호만 나오도록 endpage숫자가 총 페이지 수보다 클 경우 강제로 총페이지수를 입력
    if(endpage > totalpage) {
       endpage = totalpage;
    }    

%>
</table>
<br>
<br>
<br>
<table cellspacing=1 width=600 style="font-size:20;">
<tr>

<%
//페이지 번호 출력 식  
    if(startpage >=1){
       out.println("<td width=80><a href = list.jsp?togo="+ 0 +">"+" "+ first +" "+"</a></td>");
    }
    //이전
    if(start>=0){
       out.println("<td width=80><a href=list.jsp?togo="+ (start -datalist) +">"+" "+ before +" "+"</a></td>");
    }
	//출력될 페이지 번호 계산
     for(int i = startpage; i <= endpage; i++){
       if (i == start){          
         out.println("<td width=200><a href = list.jsp?togo="+ (i-1)*datalist +"><b>"+" "+i+" "+"</b></a></td>");
       }        
       else {
       out.println("<td width=200><a href = list.jsp?togo="+ (i-1)*datalist +"><b>"+" "+i+" "+"</b></a></td>");   
    }    
    }

    //다음
    if (start <= totalpage*datalist) {    
		int next = start+datalist;
		if(next>=totaldata){
			next=(totalpage-1)*datalist;
	} 
		out.println("<td width=80><a href=list.jsp?togo="+ (next) +">"+" "+ nex +" "+"</a></td>");
    } 
	//끝
    if (endpage <= totalpage) {	
		totalpage = (totalpage-1)*datalist;
		out.print("<td width=80><a href=list.jsp?togo="+ totalpage +">"+" "+ over +" "+"</a></td>"); 
	} 		
	rset.close();
	stmt.close();
	conn.close();
} catch(SQLException e) {
      if(extractErrorCode(e)==1062){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("데이터가 중복됩니다.");
         }else if(extractErrorCode(e)==1146){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("테이블이 존재하지 않습니다.");
         }else{
            out.println("[기타] <br>");
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.");
            out.println("입력값이 없습니다.");
         }
 }  
%>
<td width=80></td><td><input align=right type=submit onClick=location.href='register.jsp'
		value = 신규등록></input></td>
</tr>

</table>
</body>
</html>