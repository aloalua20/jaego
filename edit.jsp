<!- 재고 수정 ->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType = "text/html; charset=utf-8"%>
<%@ page import="java.sql.*,javax.sql.*, java.io.*"%>
<html>
<title>재고 수정</title>
<head>
<%
//날짜 받아오기
 java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd");
 String today = formatter.format(new java.util.Date());

 //out.println(today);

%>

<style type = "text/css">
  table {
	margin:auto;
    text-align: center;
    
    border: 5px 
  
  }
h2 {
   text-align:center;
}   
</style>
<script type="text/javascript">
<!--뒤로가기 메서드-->
function goBack(){
window.history.back();
}
<!--숫자만 입력 메서드-->
function onlyNumber(event){
	event = event || window.event;
	var keyID = (event.which) ? event.which : event.keyCode;
	if ( (keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39 ) 
		return;
	else
		return false;
}
function removeChar(event) {
	event = event || window.event;
	var keyID = (event.which) ? event.which : event.keyCode;
	if ( keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39 ) 
		return;
	else
		event.target.value = event.target.value.replace(/[^0-9]/g, "");
}
</script>
</head>

<%

//MySQL드라이버 로딩: class클래스의 forName()메소드를 이용하여 interface driver 로드
	Class.forName("com.mysql.jdbc.Driver");
//MySQL Connection 객체 생성
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/seradatabase", "root","aeae");
//sql쿼리 생성/실행을 위한 statement객체 생성
	Statement stmt = conn.createStatement();
//post 방식으로 파라메터 가져올 때 한글 사용 코드
	request.setCharacterEncoding("UTF-8");
	String id = request.getParameter("id");
//select sql쿼리 실행 결과물을 담는 ResultSet rset 생성
ResultSet rset = stmt.executeQuery("select * from jaego where id = "+id+";");
	rset.next();
	String name = rset.getString(2);
	Integer no = rset.getInt(3);
	String productDay = rset.getString(5);
	String content = rset.getString(6);
	String path = "/file/"+rset.getString(7);

%>
<br>
<h2>㈜트와이스 재고 현황 - 재고 수정</h2>
<table border = 1 cellpadding=5>
<form method=post action='write.jsp' enctype="multipart/form-data">
<!-- info에 edit값을 넣어 write.jsp로 전달 -->
<input type=hidden name="info" value="edit">
<input type=hidden name="id" value="<%=id%>">
	<tr>
	<td width=120>상품 번호 </td></td>
	<td width=480 align=left><%=id%></td>
	</tr>
	<tr>
	<td width=120>상품명 </td>
	<td width=480 align=left><%=name%></input></td>
	</tr>
	<tr>
	<td width=120>재고현황 </td>
	<td width=480 align=left><input type=text required name="no" value=<%=no%> onkeydown='return onlyNumber(event)' 
	onkeyup='removeChar(event)' style='ime-mode:disabled;'></span></td>
	</tr>
	<tr>
	<td width=120>상품등록일 </td>
	<td width=480 align=left><input type='hidden' name="productDay" value=<%=productDay%>><%=productDay%></td>
	</tr>
	<tr>
	<!-- 재고 등록일은 오늘 날짜로 변경 -->
	<td width=120>재고등록일 </td>
	<td width=480 align=left><input type='hidden' name="stockDay" value=<%=today%>><%=today%></td>
	</tr>
	<tr>
	<td width=120>상품설명 </td>
	<td width=480 align=left><input type='hidden' name="content" value=<%=content%>><%=content%></td>
	</tr>
	<tr>
	<td width=120>상품사진 </td>
	<td width=480 align=left>
	<img src=<%=path%> onerror="this.style.display='none'" alt="" />
	</td>
	</tr>
</table>
<br>
<table>
	<tr>
	<td><input type=button onClick='goBack();' value = '취소'> </input></td>
	<td>
	<td><input type=submit  value = '재고수정'></input></td>
<!-- form action='주소?인자 전달' /submit 후 onClick 생략가능,
form action='주소'에 인자 전달 하지 않을 경우 submit 후 onClick으로 인자 전달 -->
	</form>
	</tr>
</table>
<%
	rset.close();
	stmt.close();
	conn.close();

%>
</body>
</html>