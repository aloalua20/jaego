<!- 제품상세 ->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType = "text/html; charset=utf-8"%>
<%@ page import="java.sql.*,javax.sql.*, java.io.*"%>
<html>
<title>상품상세</title>
<head>
<!--에러 잡는 try-catch문-->
<%!      
   public static int extractErrorCode (SQLException sqlException) {
      int errorCode = sqlException.getErrorCode();
      SQLException nested = sqlException.getNextException();
      while(errorCode==0&&nested!=null){
         errorCode=nested.getErrorCode();
         nested=nested.getNextException();
      }
      return errorCode;
   }
%>
<style type = "text/css">
  table {
	margin:auto;
    text-align: center;

    border: 5px 
  
  }
h2 {
   text-align:center;
}   
</style>
</head>

<%
try{
//MySQL드라이버 로딩: class클래스의 forName()메소드를 이용하여 interface driver 로드
	Class.forName("com.mysql.jdbc.Driver");
//MySQL Connection 객체 생성
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/seradatabase", "root","aeae");
//sql쿼리 생성/실행을 위한 statement객체 생성
	Statement stmt = conn.createStatement();

//select sql쿼리 실행 결과물을 담는 ResultSet rset 생성
	//get방식이므로 파라메터를 String으로 받아옴
	String ids = request.getParameter("id");
	Integer id = 0;

   if(ids == null || ids.trim().equals("")){
	ids = "1";
   }
   id = Integer.parseInt(ids);
	ResultSet rset = stmt.executeQuery("select * from jaego where id = "+id+";");
	rset.next();
	String name = rset.getString(2);
	Integer no = rset.getInt(3);
	Date stockDay = rset.getDate(4);
	Date productDay = rset.getDate(5);
	String content = rset.getString(6);
	String path = "/file/"+rset.getString(7);
	
%>
<br>
<h2>㈜트와이스 재고 현황 - 상품 상세</h2>
<br>
<table border = 1 cellpadding=5>
<!--form의 post방식으로 input name/value을 파라메터로 전달하지만 
input type 이 hidden이어서 input칸 보이지않음 -->
<form method='post' action='edit.jsp?id=<%=id%>'>
	<tr>
	<td width=120>상품 번호 </td>
	<td width=480 align=left><input type='hidden' name="id" value=<%=id%>><%=id%></td>
	</tr>
	<tr>
	<td width=120>상품명 </td>
	<td width=480 align=left><input type='hidden' name="name" value=<%=name%>><%=name%></td>
	</tr>
	<tr>
	<td width=120>재고현황 </td>
	<td width=480 align=left><input type='hidden' name="no" value=<%=no%>><%=no%></td>
	</tr>
	<tr>
	<td width=120>상품등록일 </td>
	<td width=480 align=left><input type='hidden' name="productDay" value=<%=productDay%>><%=productDay%></td>
	</tr>
	<tr>
	<td width=120>재고등록일 </td>
	<td width=480 align=left><input type='hidden' name="stockDay" value=<%=stockDay%>><%=stockDay%></td>
	</tr>
	<tr>
	<td width=120>상품설명 </td>
	<td width=480 align=left><input type='hidden' name="content" value=<%=content%>><%=content%></td>
	</tr>
	<tr>
	<td width=120>상품사진 </td>
	<td width=480 align=left>
	<img src=<%=path%> onerror="this.style.display='none'" alt="" />
	</td></tr>
</table>
<br>
<table>
	<tr>
	<td>
	<input type=submit value = '재고수정'> 
	</input></td>
	<td>
	<input type=button onClick=location.href='delete.jsp?id=<%=id%>&name=<%=name%>' value = '상품삭제'></input></td>
	<td>
	<input type=button onClick=location.href='list.jsp' value = '목록'>
	</input></td>
	</tr>
	
</form>
</table>
<%
	rset.close();
	stmt.close();
	conn.close();
} catch(SQLException e) {
      if(extractErrorCode(e)==1062){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("데이터가 중복됩니다.");
         }else if(extractErrorCode(e)==1146){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("테이블이 존재하지 않습니다.");
         }else{
            out.println("[기타] <br>");
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.");
            out.println("입력값이 없습니다.");
         }
}
	
%>
</table>
</html>